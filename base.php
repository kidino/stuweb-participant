<?php
	session_start();
	///////////////////// Do not edit anything below this line
	// $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkOTFkZjc5Yjk3NTgxZjdlOWI3OS00YzkxNDIyN2EwZWE1YWMwNWExYWYyZDM2NzgwYTZjNGE3YmIyMTBjNGNmOGFkOWQtYWZhMjRjYmZiYiIsImlhdCI6MTUyNjUyMDQ1MCwiZXhwIjoxNTI2NTI0MDUwLCJuYW1lIjoic3lhcml6YW4ifQ.0aAHACff_VcEAW8kfRfwvwONHD_cYYUoC58F3pNRqYY';
	
	$token = $_SESSION['token'];
	
	$api_base_url = 'http://training.moha.gov.my/api/';
	
	$ch = curl_init($api_base_url.'students');
	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer '.$token,
			//'Content-Length: ' . strlen($data_string),
			'User-Agent: Student App'
		)
	);
	
	$result = curl_exec($ch);
	curl_close($ch);
	
	$array = json_decode($result, true);
	
	
	//echo "<pre>";
	//print_r($array);
	//echo "</pre>";
	///////////////////// Do not edit anything above this line

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
	  .container { max-width: 800px; }
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 well text-center">
				<h1>Student List</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<span><a style="margin-bottom: 5px" href="base.php" class="btn btn-sm btn-warning">List</a></span>
				<span><a style="margin-bottom: 5px" href="add.php" class="btn btn-sm btn-warning">New Student</a></span>
				<span><a style="margin-bottom: 5px" href="logout.php" class="btn btn-sm btn-danger">Logout</a></span>
				<table id="myTable" class="table table-bordered table-striped" >
					<thead>
						<tr>
							<td align='center'>No</td>
							<td align='center'>Student No</td>
							<td align='center'>Forename</td>
							<td align='center'>Surname</td>
							<td align='center'>Action</td>
						</tr>
					</thead>
					<tbody>
						<?php 
						$x = 1;
						foreach($array['entry'] as $row){ ?>
						<tr>
							<td align="right"><?= $x ?>.</td>
							<td><?= $row["student_no"] ?></td>
							<td><?= $row["forename"] ?></td>
							<td><?= $row["surname"] ?></td>
							<td align="center" width="20%">
								<a href="update.php?student_no=<?=$row['student_no']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil-alt"></i></a>
								<a href="del.php?student_no=<?=$row['student_no']; ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-alt"></i></a>
							</td>
						</tr>
						<?php 
						$x++; 
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>	
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
  

</body>
</html>
	<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>