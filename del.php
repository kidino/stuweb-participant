<?php
	session_start();

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<style>
	  .container { max-width: 800px; }
	</style>
</head>
<body>
	<div class="container">
<?php
	///////////////////// Do not edit anything below this line
//	$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkOTFkZjc5Yjk3NTgxZjdlOWI3OS00YzkxNDIyN2EwZWE1YWMwNWExYWYyZDM2NzgwYTZjNGE3YmIyMTBjNGNmOGFkOWQtYWZhMjRjYmZiYiIsImlhdCI6MTUyNjUyMDQ1MCwiZXhwIjoxNTI2NTI0MDUwLCJuYW1lIjoic3lhcml6YW4ifQ.0aAHACff_VcEAW8kfRfwvwONHD_cYYUoC58F3pNRqYY';

	$token = $_SESSION['token'];

	
	$api_base_url = 'http://training.moha.gov.my/api/';
	$student_no = $_GET['student_no'];

	$ch = curl_init($api_base_url.'students/'.$student_no);
	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Authorization: Bearer '.$token,
			//'Content-Length: ' . strlen($data_string),
			'User-Agent: Student App'
		)
	);
	
	$result = curl_exec($ch);
	curl_close($ch);

if($result){ 

?>
	<div class="alert alert-success">
	<strong>Success!</strong> Student Have been Deleted.
	</div>
	<meta http-equiv="refresh" content=1;URL="base.php">
<?php }else{ ?>
	<div class="alert alert-warning">
	<strong>Failed!</strong> Delete unsuccessful.
	</div>
	<meta http-equiv="refresh" content=1;URL="base.php">	
<?php }  ?>
	</div>
</body>        
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>	
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>