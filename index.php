<?php
session_start();
include('functions.php');

if (isset($_SESSION['token'])) {
	header('Location: base.php');
}

if (isset($_POST['username'])) {
	$api = new MOHA_API();
	
	$data = array(
		'username' => $_POST['username'],
		'password' => $_POST['password']
	);
	
	$result = $api->go('consumer/login','POST',$data);
	
	if (isset($result['token'])) {
		$_SESSION['token'] = $result['token'];
		header('Location: base.php'); exit();
	} else {
		header('Location: index.php?error=invalid');
	}
}

?>
<?php include('header.php'); ?>
		
		<div id="login-form">
			<div class="row">
				<div class="col-md-12">
				
<?php if (isset($_GET['error']) && ($_GET['error'] == 'invalid')) { ?>
<p class="alert alert-danger">Invalid login details. Please try again.</p>
<?php } ?>
				
<?php if (isset($_GET['error']) && ($_GET['error'] == 'expired')) { ?>
<p class="alert alert-danger">Login token has expired. Please login again.</p>
<?php } ?>
				
					<div class="well">
						
						
<form class="form-horizontal" method="post" action="index.php">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" name="username" placeholder="Username">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
    </div>
  </div>
					
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" id="login-btn">Sign in</button>
    </div>
  </div>
</form>						
						
					</div>
				</div>
			</div>
			
		</div>

<?php include('footer.php'); ?>