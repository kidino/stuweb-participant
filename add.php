<?php
	session_start();

if(isset($_POST['action'])){

// $token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmOWZlZDg4ZTU3NmYzZTBhZGFjNS0zOGU4MTJkZjFjOTYzMDZjMzg1OWEwYzI4MGVkNjAzYmRiM2RmNmQyNDdkNTdjZGMtOGVlZDE4NzQwNSIsImlhdCI6MTUyNjUyMzEzNiwiZXhwIjoxNTI2NTI2NzM2LCJuYW1lIjoic3l1a3VyIn0.jTK9AkqbyTxU4NZdLzwCbOMhFgZqRaYzuZc3qNeEHng';

	$token = $_SESSION['token'];


 $student_no=$_POST['student_no'];
 $surname=$_POST['surname'];
 $forename=$_POST['forename'];

 //  $student_no="123444";
 // $surname="Dino";
 // $forename="Kareem";


 $data=array('student_no'=>$student_no,'surname'=>$surname,'forename'=>$forename);


 $data_string = json_encode($data);
 $api_base_url = 'http://training.moha.gov.my/api/';

 $ch = curl_init($api_base_url.'students');

 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
 curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/json',
  'Content-Length: '.strlen($data_string),
  'User-Agent: Student App',
  'Authorization: Bearer '. $token
    )
  );

 $result = curl_exec($ch);
 $data = json_decode($result);
//echo $result;
 
 //print_r($result);

 //echo $data->success;

 //$_SESSION['token'] = $data->token; 


}  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>
    .container { max-width: 800px; }
</style>
</head>
<body>
  
  <div class="container">
    <div class="row">
      <div class="col-md-12 well text-center">
        <h1>ADD STUDENT</h1>
      </div>
  </div>
  
  				<span><a style="margin-bottom: 5px" href="base.php" class="btn btn-sm btn-warning">List</a></span>
				<span><a style="margin-bottom: 5px" href="add.php" class="btn btn-sm btn-warning">New Student</a></span>
				<span><a style="margin-bottom: 5px" href="logout.php" class="btn btn-sm btn-danger">Logout</a></span>
<hr>

  <?php
   //var_dump($data);

    // if($data->success==false){
    if(isset($data) && ($data->success==true)) { ?>
      <div class="alert alert-success" role="alert"><?php echo $data->message; ?></div>
	<meta http-equiv="refresh" content=2;URL="base.php">
    <?php } else if (isset($data) && ($data->success==false)) { ?>
      <div class="alert alert-danger" role="alert"><?php echo $data->message; ?></div>
    <?php } ?>  

  <form class="form-horizontal" action="add.php" method="POST" >
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Student No :</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="student_no" name="student_no" placeholder="Student Number">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Surname:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="surname" name="surname" placeholder="surname">
    </div>
  </div>
   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Forename:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="forename" name="forename" placeholder="forename">
    </div>
  </div>
  <!-- <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
    </div>
  </div> -->
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </div>
 <input type="hidden" class="form-control" id="action" name="action"> 
</form>
    
    
    
  </div>
  
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</body>
</html>


<script id="list-tpl" type="text/template">
<tr id="todo-{{id}}">
  <td>{{title}}</td>
  <td width="5"><button class="btn btn-xs btn-danger" onclick="todo_delete({{id}})"><span class="glyphicon glyphicon-remove"></span></button></td>
</tr>
</script>










