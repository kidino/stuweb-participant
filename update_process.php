<?php
session_start();
//UPDATE DATA

// $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2NjBkOWU1MjE4ZjlkNjdlNDYyMS0xZGQ1OGQ4NjgyOTc0ZjAxYzBjNWM4M2EwMWJlYjNmYmRhMzVhYTEyOTZlOTU5NWMtZDc3NTg0M2VjYSIsImlhdCI6MTUyNjUyNDM2MywiZXhwIjoxNTI2NTI3OTYzLCJuYW1lIjoiamFuYXJ0aGFuIn0.rAisoTEnoGXDhRiqNEpiA1wbKoOVZWQfP-41g5SuTL0';

$token = $_SESSION['token'];

if(isset($_POST['student_no'])){
	$student_no = $_POST['student_no'];
} else {
	$student_no = '';
}

if(isset($_POST['surname'])){
	$surname = $_POST['surname'];
} else {
	$surname = '';
}

if(isset($_POST['forename'])){
	$forename = $_POST['forename'];
} else {
	$forename = '';
}

//if data not complete show error and redirect page
if(empty($student_no)||empty($surname)||empty($forename)){ echo "FAIL"; 
header('Location: update.php?student_no='.$student_no.'&msg=DATA INCOMPLETE');
exit;
}



$postdata = array("student_no"=>$student_no, "surname"=>$surname, "forename"=>$forename);

//print_r($data);

if ($postdata === null) {
header('Location: update.php?student_no='.$student_no.'&msg=DATA INCOMPLETE');
exit;
	//echo "error";
	//exit();
}

$data_string = json_encode($postdata, true);

$base_api_url = 'http://training.moha.gov.my/api/';
$update_url = $base_api_url . 'students/'.$student_no;
$ch = curl_init($update_url);
	
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
               array('Content-Type:application/json',
				'User-Agent: Student App',
				'Authorization: Bearer '.$token,
				'Content-Length: '.strlen($data_string)
               ));

    $result = curl_exec($ch);
    curl_close($ch);
	
	if($result){$msg='DATA UPDATED';}else{$msg='UPDATE FAIL';}
	header('Location: update.php?student_no='.$student_no.'&msg='.$msg);


//data = json_decode($result, true);




//print_r($jsondata); die();



?>