<?
	session_start();
$token = $_SESSION['token'];

$student_no = $_GET['student_no'];

if ($student_no == '') {
	header('Location: base.php');
}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>
	  .container { max-width: 800px; }
</style>
</head>
<body>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12 well text-center">
				<h1>update</h1>
			</div>
		</div>
		
				<span><a style="margin-bottom: 5px" href="base.php" class="btn btn-sm btn-warning">List</a></span>
				<span><a style="margin-bottom: 5px" href="add.php" class="btn btn-sm btn-warning">New Student</a></span>
				<span><a style="margin-bottom: 5px" href="logout.php" class="btn btn-sm btn-danger">Logout</a></span>
<hr>		
	</div>
	
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>	
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<?php

//GET ONE RECORD

//$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2NjBkOWU1MjE4ZjlkNjdlNDYyMS0xZGQ1OGQ4NjgyOTc0ZjAxYzBjNWM4M2EwMWJlYjNmYmRhMzVhYTEyOTZlOTU5NWMtZDc3NTg0M2VjYSIsImlhdCI6MTUyNjUyNDM2MywiZXhwIjoxNTI2NTI3OTYzLCJuYW1lIjoiamFuYXJ0aGFuIn0.rAisoTEnoGXDhRiqNEpiA1wbKoOVZWQfP-41g5SuTL0';


$base_api_url = 'http://training.moha.gov.my/api/';

$get_url = $base_api_url . 'students/'.$student_no;

$ch = curl_init($get_url);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,
               array('Content-Type:application/json',
				'User-Agent: Student App',
				'Authorization: Bearer '.$token
               ));

    $result = curl_exec($ch);
    curl_close($ch);

//print_r($result);

$data = json_decode($result, true);

if ($data === null) {
	echo "error";
	exit();
}

var_dump($token);
print_r($data);



?>
<!--<form method="POST" action="update_process.php"> -->
<?php 
/*
if(isset($_GET['msg'])){
	if($_GET['msg']==1){
		echo "<font color=\"red\">DATA NOT COMPLETE</font>";
	}elseif($_GET['msg']==2){
		echo "<font color=\"red\">DATA UPDATED</font>";
	}else{
		echo "<font color=\"red\">UPDATE FAIL</font>";
	}
}*/
//if(isset($_GET['msg'])){
	//echo "<font color=\"red\">".$_GET['msg']."</font>";
//}
	?>
	<!--
<div>
<label>Student No : </label><input type="text" name="student_no" id="student_no" value="<?= $data['student_no'] ?>" ><br>
<label>Surname : </label><input type="text" name="surname" id="surname" value="<?= $data['surname'] ?>"><br>
<label>Forename : </label><input type="text" name="forename" id="forename" value="<?= $data['forename'] ?>"><br>
</div>
<br>
<input type="submit" name="update">

</form> -->


<div class="container">
<?php if(isset($_GET['msg'])){
	echo "<font color=\"red\">".$_GET['msg']."</font>";
}
?>
<form method="POST" action="update_process.php">
  <div class="form-group">
    <label>Student No :</label>
    
	<input type="text" class="form-control" name="student_no" id="student_no" value="<?= $data['student_no'] ?>" >
  </div>
  <div class="form-group">
    <label>Surname :</label>
    
	<input type="text" class="form-control" name="surname" id="surname" value="<?= $data['surname'] ?>">
  </div>
    <div class="form-group">
    <label>Forename :</label>
    
	<input type="text" class="form-control" name="forename" id="forename" value="<?= $data['forename'] ?>">
  </div>


  <input type="submit" class="btn btn-default" name="update">
</form>
</div>

</body>
</html>


<script id="list-tpl" type="text/template">
<tr id="todo-{{id}}">
	<td>{{title}}</td>
	<td width="5"><button class="btn btn-xs btn-danger" onclick="todo_delete({{id}})"><span class="glyphicon glyphicon-remove"></span></button></td>
</tr>
</script>